﻿public static class BGM
{

    static AudioData _audioData;
    static AudioData audioData { get { return _audioData = _audioData ?? (_audioData = Resources.Load<AudioData>("AudioData")); } }

    public static AudioSource 01 TOMBTORIAL (TUTORIAL) { get { return audioData.GetBGM("01 Tombtorial (Tutorial)"); } }
}
