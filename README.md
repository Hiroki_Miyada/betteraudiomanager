# BetterAudioManager #

再生準備の自動化などを実現するちょっとましなオーディオマネージャーです。

## セットアップ ##

### 1.ダウンロード、インポート ###

[こちら](https://bitbucket.org/Hiroki_Miyada/betteraudiomanager/raw/2d1c5a3b031dd916191a25190bc526cdd5fab642/BetterAudioManager.unitypackage)からパッケージのダウンロード、インポートを行って下さい。

![BAM00.png](https://bitbucket.org/repo/LjkEzL/images/4090335565-BAM00.png)

### 2.AudioData の作成 ###

Assets/Resources でコンテキストクリックし、 Create > Create AudioData を選択してください。

![BAM01.png](https://bitbucket.org/repo/LjkEzL/images/1801165011-BAM01.png)

### 3.必要なファイルのインポート ###

Assets/Audio 以下の BGM, SE フォルダに使用するオーディオデータを、 MixerGroup にオーディオミキサーを配置してください。  
呼び出し用のコードが生成されます。

![BAM02.png](https://bitbucket.org/repo/LjkEzL/images/2565509164-BAM02.png)

## 使用例 ##

```
#!c#

AudioManager.Instance.PlayBGM(BGM._01_Tombtorial_Tutorial_, MixerGroup.BGM);
```

## LICENSE ##

Copyright (c) 2015 Hiroki Miyada
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.